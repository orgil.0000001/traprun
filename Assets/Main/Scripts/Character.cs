using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class LbData {
    public string name;
    public int scr, scr2, kill;
    public Color col;
    public bool isLive, isPlayer;
    public LbData(string name, Color col, bool isPlayer) { Set(name, 0, 0, 0, col, true, isPlayer); }
    public void Set(string name, int scr, int scr2, int kill, Color col, bool isLive, bool isPlayer) {
        this.name = name;
        this.scr = scr;
        this.scr2 = scr2;
        this.kill = kill;
        this.col = col;
        this.isLive = isLive;
        this.isPlayer = isPlayer;
    }
}
public class Character : Mb {
    public LbData lb;
    protected Ragdoll rd;
    protected float run = 0;
    public void Lb(bool isPlayer) {
        lb = isPlayer ? new LbData(Data.Player.S(), O.Cols[0], true) : new LbData(O.Names.Rnd(), O.Cols.RndI(0), false);
    }
    public void Init() {
        rd = new Ragdoll(go);
        rb.Con(false, true, false, true, true, true);
    }
    public void Run() {
        if (run < 1) {
            run += Dt / 0.3f;
            if (run > 1) run = 1;
            rd.an.SetFloat("Run", run);
        }
    }
}
