public class Singleton<T> : Mb where T : Mb {
    public static T _ { get { if (!__) __ = (T)FindObjectOfType(typeof(T)); return __; } }
    static T __;
}