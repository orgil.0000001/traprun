namespace Orgil {
	public enum Tag { Untagged, Respawn, Finish, EditorOnly, MainCamera, Player, GameController, FxTemporaire, FX, World, Target, Bullet, Button, Dead, Door }
	public class Lyr { public const int Default = 0, TransparentFX = 1, IgnoreRaycast = 2, Bot = 3, Water = 4, UI = 5, Bot2 = 6; }
	public class Lm { public const int Default = 1, TransparentFX = 2, IgnoreRaycast = 4, Bot = 8, Water = 16, UI = 32, Bot2 = 64; }
}