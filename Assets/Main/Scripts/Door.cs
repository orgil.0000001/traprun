using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class Door : Mb {
    public ObsTp tp;
    public bool isR;
    public void Init(ObsTp tp, float z, bool isR) {
        Tp = V3.V(isR.Sign() * 2.5f, 0, z);
        this.tp = tp;
        this.isR = isR;
        go.ChildShow((int)tp / 2);
    }
    public void Enter() {
        var l = O.FOsOT<Door>().FindAll(door => M.Apx(door.Tp().z, Tp.z));
        Sound.Play(tp.tS().Rmv("Shard"));
        if (l.Count == 2) tp++;
        GC.CrtObs(tp, Tp.z, isR);
        l.ForEach(door => Dst(door.go));
    }
    void Update() {
        if (P.Tp.z - 20 > Tp.z)
            Dst(go);
    }
}
