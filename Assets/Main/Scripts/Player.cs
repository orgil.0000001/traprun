﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;
using KenneyCharacterAssets;

public class Player : Character {
    public static Player _ { get { if (!__) __ = (Player)FindObjectOfType(typeof(Player)); return __; } }
    static Player __;
    public float spd = 5, swipe = 5;
    public GameObject pntGo;
    Vector2 dMp => V2.V((Mp.x - mp.x) / (Screen.width * 0.3f), (Mp.y - mp.y) / (Screen.width * 0.2f));
    int roadIdx = 0;
    bool isWin = false;
    void Start() {
        Init();
    }
    public void MbD() {
        mp = Mp;
    }
    void Update() {
        if (IsGame && !isWin) {
            if (IsMbD) {
                MbD();
            }
            if (IsMb) {
                Tp = Tp.X(M.C(Tp.x + dMp.x * swipe, -GC.roadR, GC.roadR));
                mp = Mp;
            }
            if (IsMbU) {
            }
            Run();
            rb.V(V3.Z(run * spd));
        }
    }
    public void Revive() {
    }
    void Dead() {
        rb.ConFrzAll();
        rb.V0();
        rd.On();
        O.FOsOT<Bot>().ForEach(bot => { if (bot.isFollow) bot.Win(); });
        CC.Fail();
    }
    IEnumerator WinCor(GameObject finishGo) {
        isWin = true;
        O.FOsOT<Bot>().ForEach(bot => { if (bot.isFollow) bot.Dead(); });
        GameObject confettiGo = finishGo.ChildGo(3);
        confettiGo.Show();
        float finishSz = finishGo.Child(1).Tls().z, bonusSz = finishGo.Child(2, 0).Tls().z;
        Vector3 pos = finishGo.TfPnt(V3.Z(finishSz - 1));
        while (V3.Dis(Tp, pos) > 0.5f) {
            Tr = Q.Y(Ang.Xz(Tp, pos));
            rb.V(TfF * spd);
            yield return null;
        }
        rb.V0();
        Tp = pos;
        Quaternion rot = Tr;
        rd.an.SetBool("JumpInit", true);
        yield return Cor(t => Tr = Q.Lerp(rot, Q.O, t), 0.3f);
        StaCor(SclCor(pntGo.ParGo(), V3.O, V3.I, 0.2f, EaseTp.OutSine));
        int x = 1;
        for (float t = 0, lim = 310; true; t += Dt) {
            if (IsMbD) {
                StaCor(SclCor(pntGo.ParGo(), V3.I, V3.O, 0.2f, EaseTp.InSine));
                rd.an.SetBool("Jump", true);
                rb.ConFrzRot();
                x = Lis(1, 2, 3, 4, 5, 4, 3, 2, 1)[M.RoundI(pntGo.Tlp().x / lim * 4 + 4)];
                float tm = 0, ang = 0;
                pos = finishGo.TfPnt(V3.Z(finishSz + (x - 0.5f) * bonusSz));
                rb.V(Hpm.H_V0(Tp, pos, M.Remap(x, 1, 5, 2, 8), ref tm, ref ang));
                yield return Wf.Sec(tm);
                confettiGo.Tp(pos);
                confettiGo.HideShow();
                Tp = pos;
                rb.ConFrzAll();
                rd.an.Play("JumpEnd");
                break;
            }
            pntGo.TlpX(M.Lerp(-lim, lim, M.Sin01(t * 180)));
            yield return null;
        }
        float rate = M.Remap(x, 1, 5, 0.7f, 1) + (x == 5 ? 0 : Rnd.F1 * 0.05f);
        CC.Complete(rate, M.RemapI(rate, 0.7f, 1, 30, 100));
    }
    void OnTriggerEnter(Collider other) {
        if (IsGame)
            if (other.name == "Finish") {
                StaCor(WinCor(other.gameObject));
            } else if (other.Tag(Tag.Dead) && other.name == "Saw") {
                Dead();
            } else if (other.Tag(Tag.Door)) {
                other.Par<Door>().Enter();
            } else if (other.Tag(Tag.Button)) {
                Obs obs = other.Par<Obs>();
                Sound.Play(obs.tp.tS());
                if (obs.tp == ObsTp.FlameThrower || obs.tp == ObsTp.Laser) {
                    obs.ChildShow(1);
                } else if (obs.tp == ObsTp.Glove) {
                    StaCor(GloveCor(obs));
                } else if (obs.tp == ObsTp.Explosion) {
                    StaCor(ExplosionCor(obs));
                }
                StaCor(BtnCor(other));
            }
    }
    void OnCollisionEnter(Collision other) {
        if (IsGame)
            if (other.Par().name.IsS(ObsTp.Ball.tS())) {
                Sound.Play("Ball");
                other.Sib(3).RbNoK();
            } else if (other.gameObject.name.IsS("Bot")) {
                Dead();
            }
    }
    IEnumerator GloveCor(Obs obs) {
        GameObject go = obs.ChildGo(0);
        go.ChildShow(1);
        float a = go.Tp().x, b = obs.isR ? -10 : 10;
        yield return Cor(t => go.TpX(M.Lerp(a, b, t)), 1);
    }
    IEnumerator ExplosionCor(Obs obs) {
        obs.ChildHide(0);
        obs.ChildShow(1);
        yield return Wf.Sec(2);
        if (obs)
            obs.ChildHide(1);
    }
    IEnumerator BtnCor(Collider other) {
        other.Col().enabled = false;
        GameObject go = other.ChildGo(0);
        yield return Cor(t => go.TlpY(t * -0.15f), 0.2f);
    }
}
// void GoingBallsMv(float spd, float angSpd) {
//     if (IsGame) {
//         if (IsMbD)
//             mp = Mp;
//         if (IsMb) {
//             float dis = V3.Dis(mp, Mp);
//             if (dis > 10) {
//                 float ang = Ang.Xy(mp, Mp);
//                 Tle = V3.Y(Ang.Lerp(Tle.y, Cm.I.Tle.y + ang, Dt * angSpd));
//                 mp = Mp;
//                 rb.AddForce(F * dis * spd / 100f);
//             }
//         }
//     }
// }
// void BotRndMv(ref float curAng, ref float ang, ref float t, ref float dt, float spd = 10, float rotSpd = 5, float minTm = 1.5f, float maxTm = 3) {
//     if (IsGame) {
//         dt += Dt;
//         if (dt > t) {
//             dt = 0;
//             t = Rnd.Rng(minTm, maxTm);
//             ang = Rnd.Ang;
//         }
//         curAng = M.Lerp(curAng, ang, Dt * rotSpd);
//         Tr = Q.Y(curAng);
//         rb.V(F * spd);
//     } else
//         rb.V0();
// }
// void HoleIoMv(float spd = 10, float r = 100) {
//     if (IsGame) {
//         if (IsMbD)
//             mp = Mp;
//         if (IsMb) {
//             float dis = V3.Dis(Mp, mp);
//             if (dis > r)
//                 mp = V3.Mv(Mp, mp, r);
//             Tr = Q.Y(Ang.Xy(mp, Mp));
//             rb.V(F * M.C01(dis / r) * spd);
//         }
//         if (IsMbU)
//             rb.V0();
//     } else
//         rb.V0();
// }