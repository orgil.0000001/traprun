﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Orgil;

public enum GameState { Menu, Game, Complete, Fail, Settings }
public enum Data { IsWin, IsHaptic, IsSound, Level, Coin, Best, LevelData, Settings, BtnLvls, Player, Time, Skin, SkinCnt }
public class Gc : Singleton<Gc> { // Game Controller
    public static GameState State;
    public static bool IsWin { get => Data.IsWin.B(); set => Data.IsWin.B(value); }
    public static bool IsHaptic { get => Data.IsHaptic.B(); set => Data.IsHaptic.B(value); }
    public static bool IsSound { get => Data.IsSound.B(); set => Data.IsSound.B(value); }
    public static bool Settings { get => Data.Settings.B(); set => Data.Settings.B(value); }
    public static int Level { get => Data.Level.I(); set => Data.Level.I(value); }
    public static int Coin { get => Data.Coin.I(); set { Data.Coin.I(value); CC.coinTxt.text = "" + value; } }
    public static int SkinCnt { get => Data.SkinCnt.I(); set => Data.SkinCnt.I(value); }
    public static float Best { get => Data.Best.F(); set => Data.Best.F(value); }
    public static float Score, Tm;
    // bool, int, float, string, Vector2, Vector2Int, Vector3, Vector3Int, Vector4
    public static Dictionary<Data, object> Datas = new Dictionary<Data, object>() {
        { Data.Level, 1 }, { Data.Coin, 0 }, { Data.Best, 0f }, { Data.Time, 0 }, { Data.SkinCnt, 0 },
        { Data.IsWin, true }, { Data.IsHaptic, true }, { Data.IsSound, true }, { Data.Settings, false },
        { Data.LevelData, "" }, { Data.BtnLvls, "" }, { Data.Player, "Player" }, { Data.Skin, ""}
    };
    public int lvl = 0;
    public Bot botPf;
    public Door doorPf;
    public Road roadPf;
    public GameObject btnPf, lightningStrikePf;
    public List<GameObject> barrierPfs;
    public List<Obs> obsPfs;
    public float roadW = 5, roadBor = 5.3f, humanR = 0.5f, roadR = 4.5f;
    public List<Color> bonusCols;
    int crtObsIdx = 0, crtRoadIdx, obsIdx = 0;
    bool isBarrier2 = false;
    float len;
    Transform roadTf, botTf, doorTf, obsTf;
    List<Bot> bots = new List<Bot>();
    List<GameObject> roadGos = new List<GameObject>();
    void Awake() {
        IO.Init();
        Score = 0;
        Tm = 120;
        if (lvl > 0)
            Level = lvl;
    }
    void Start() {
        Fog();
        roadTf = go.Child(0);
        botTf = go.Child(1);
        doorTf = go.Child(2);
        obsTf = go.Child(3);
        GameObject roadGo = roadTf.ChildGo(0), finishGo = roadTf.ChildGo(1), bonusPf = finishGo.ChildGo(2);
        len = M.Remap(Level, 1, 100, 200, 500);
        float roadSz = len - roadGo.Tlp().z;
        finishGo.TlpZ(len);
        roadGo.TlsZ(roadSz);
        roadGo.Child(1).RenMat().mainTextureScale = V2.V(1, roadSz / 10);
        roadGo.Child(1).Mf().sharedMesh = QuadMesh(100);
        roadGo.Child(0).Mf().sharedMesh = roadGo.Child(2).Mf().sharedMesh = roadGo.Child(3).Mf().sharedMesh = CubeMesh(100);
        float finishSz = finishGo.ChildGo(1).Tls().z, bonusSz = bonusPf.Child(0).Tls().z;
        for (int i = 0; i < 5; i++) {
            GameObject bGo = i == 0 ? bonusPf : Ins(bonusPf, bonusPf.Par());
            bGo.TlpZ(finishSz + i * bonusSz);
            bGo.Child(0).RenMatCol(bonusCols[i]);
            bGo.Child(1).Tmp().text = (i + 1) + ".0X";
        }
        crtRoadIdx = Rnd.RngIn(1, 2);
        for (int i = 0; i < 4; i++)
            CrtRndObs();
        bots = CrtBots(50, Q.O, V3.Xz(-4.5f, -16), V3.Xz(4.5f, -7));
        P.Lb(true);
        CC.Menu();
    }
    void Update() {
        if (IsGame) {
            if (P.Tp.z > Z(obsIdx)) {
                obsIdx++;
                CrtRndObs();
            }
        }
    }
    float Z(int i) { return 30 + i * 30; }
    void CrtRndObs() {
        float z = Z(crtObsIdx), f = Rnd.F;
        if (z < len - 20) {
            if (f < 0.4f) {
                ObsTp tp = Obs.Doors.Rnd();
                if (Rnd.P(0.4f)) {
                    ObsTp tp2 = Rnd.ListE(tp, Obs.Doors);
                    CrtDoor(tp, z, false);
                    CrtDoor(tp2, z, true);
                } else {
                    CrtDoor(tp, z, Rnd.B);
                }
            } else {
                if (f < 0.8f) {
                    CrtObs(f < 0.6f ? Obs.Btns.Rnd() : ObsTp.Ball, z, Rnd.B);
                } else {
                    int tp = Rnd.N(2);
                    if (tp < 2 && Rnd.P(0.4f)) {
                        CrtSaw(tp, z, true);
                        CrtSaw(tp, z + 1, false);
                    } else {
                        CrtSaw(tp, z, Rnd.B);
                    }
                }
            }
            if (crtObsIdx == crtRoadIdx) {
                crtRoadIdx += Rnd.RngIn(1, 2);
                CrtRoad(z - 7, Rnd.B, 30);
            }
            crtObsIdx++;
        }
    }
    public void Follow() { bots.ForEach(b => b.Follow()); }
    void CrtDoor(ObsTp tp, float z, bool isR) { Ins(doorPf, doorTf).Init(tp, z, isR); }
    public void CrtObs(ObsTp tp, float z, bool isR) { Ins(obsPfs[(int)tp], obsTf).Init(tp, z, isR); }
    public void CrtSaw(int tp, float z, bool isR) { Ins(obsPfs[(int)ObsTp.Saw], obsTf).Saw(tp, z, isR); }
    void CrtRoad(float z, bool isR, int cnt) {
        Road road = Ins(roadPf, V3.V(isR.Sign() * roadBor, 0, z), Q.Y(isR ? 90 : -90), roadTf);
        road.Init(Ins(barrierPfs[isBarrier2 ? 2 : isR ? 0 : 1], road.TfPnt(V3.Z(-0.15f)), road.Tr(), road.transform),
            CrtBots(cnt, road.Tr() * Q.Y(180), road.TfPnt(V3.Xz(-roadR, humanR)), road.TfPnt(V3.Xz(roadR, 5))));
    }
    List<Bot> CrtBots(int cnt, Quaternion rot, Vector3 p0, Vector3 p1) {
        List<Bot> bots = new List<Bot>();
        List<Vector3> pnts = new List<Vector3>();
        for (int i = 0; i < 10000 && pnts.Count < cnt; i++) {
            Vector3 p = Rnd.Pos(p0, p1);
            if (pnts.FindIndex(x => V3.Dis(x, p) < 1) < 0) {
                pnts.Add(p);
                bots.Add(Ins(botPf, p, rot, botTf));
            }
        }
        return bots;
    }
    void Fog(Color col = default, float dis = 80, float spc = 10) {
        if (col.IsDef())
            col = Cam.backgroundColor;
        RenderSettings.fog = true;
        RenderSettings.fogColor = Cam.backgroundColor = col;
        RenderSettings.fogMode = FogMode.Linear;
        RenderSettings.fogStartDistance = dis;
        RenderSettings.fogEndDistance = dis + spc;
    }
    public static int GetLvl(int lvl, int lvlCnt, int rndSta, int rndEnd) {
        if (Data.LevelData.S().IsNe())
            Data.LevelData.ListI(O.LisAp(1, 1, lvlCnt));
        List<int> lis = Data.LevelData.ListI();
        if (lvl <= lis.Count) {
            return lis[lvl - 1];
        } else {
            int prvLvl = lis.Last();
            for (int i = lis.Count + 1; i <= lvl; i++) {
                prvLvl = prvLvl == rndSta ? Rnd.RngIn(rndSta + 1, rndEnd) : prvLvl == rndEnd ? Rnd.RngIn(rndSta, rndEnd - 1) : Rnd.P((prvLvl - rndSta).F() / (rndEnd - rndSta)) ? Rnd.RngIn(rndSta, prvLvl - 1) : Rnd.RngIn(prvLvl + 1, rndEnd);
                lis.Add(prvLvl);
            }
            Data.LevelData.ListI(lis);
            return prvLvl;
        }
    }
    Mesh QuadMesh(int n) {
        Mesh m = new Mesh();
        List<Vector3> vs = new List<Vector3>();
        List<int> ts = new List<int>();
        List<Vector2> uv = new List<Vector2>();
        for (int i = 0; i <= n; i++) {
            float y = i.F() / n, z = y - 0.5f;
            vs.Add(V3.V(-0.5f, z, 0), V3.V(0.5f, z, 0));
            uv.Add(V2.V(0, y), V2.V(1, y));
        }
        for (int i = 0; i < n; i++) {
            int j = i * 2;
            ts.Add(j, j + 2, j + 3, j, j + 3, j + 1);
        }
        m.vertices = vs.Arr();
        m.triangles = ts.Arr();
        m.uv = uv.Arr();
        m.RecalculateNormals();
        return m;
    }
    Mesh CubeMesh(int n) {
        Mesh m = new Mesh();
        List<Vector3> vs = new List<Vector3>();
        List<int> ts = new List<int>();
        List<Vector2> uv = new List<Vector2>();
        for (int i = 0; i <= n; i++) {
            float y = i.F() / n, z = y - 0.5f;
            vs.Add(V3.V(-0.5f, -0.5f, z), V3.V(-0.5f, 0.5f, z),
                V3.V(-0.5f, 0.5f, z), V3.V(0.5f, 0.5f, z),
                V3.V(0.5f, 0.5f, z), V3.V(0.5f, -0.5f, z),
                V3.V(0.5f, -0.5f, z), V3.V(-0.5f, -0.5f, z));
            uv.Add(V2.V(0, y), V2.V(1, y), V2.V(0, y), V2.V(1, y), V2.V(0, y), V2.V(1, y), V2.V(0, y), V2.V(1, y));
        }
        vs.Add(vs[0], vs[2], vs[4], vs[6], vs.Last(0), vs.Last(2), vs.Last(4), vs.Last(6));
        uv.Add(V2.O, V2.O, V2.O, V2.O, V2.I, V2.I, V2.I, V2.I);
        for (int i = 0; i < n; i++) {
            int j = i * 8;
            ts.Add(j, j + 8, j + 9, j, j + 9, j + 1,
                j + 2, j + 10, j + 11, j + 2, j + 11, j + 3,
                j + 4, j + 12, j + 13, j + 4, j + 13, j + 5,
                j + 6, j + 14, j + 15, j + 6, j + 15, j + 7);
        }
        int k = vs.Count - 8;
        ts.Add(k, k + 1, k + 2, k, k + 2, k + 3,
            k + 4, k + 5, k + 6, k + 4, k + 6, k + 7);
        m.vertices = vs.Arr();
        m.triangles = ts.Arr();
        m.uv = uv.Arr();
        m.RecalculateNormals();
        return m;
    }
}