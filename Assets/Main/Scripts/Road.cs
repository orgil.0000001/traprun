using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class Road : Mb {
    public GameObject barrier;
    public List<Bot> bots;
    bool isOpen = false;
    public void Init(GameObject barrier, List<Bot> bots) {
        this.barrier = barrier;
        this.bots = bots;
    }
    void Update() {
        if (!isOpen && P.Tp.z - 3 > Tp.z)
            StaCor(OpenCor());
        if (P.Tp.z - 30 > Tp.z)
            Dst(go);
    }
    IEnumerator OpenCor(float tm = 0.3f) {
        isOpen = true;
        for (int i = 0; i < barrier.Tcc() - 2; i++) {
            GameObject cGo = barrier.ChildGo(i);
            Quaternion rot = cGo.Tlr();
            StaCor(Cor(t => cGo.Tlr(Q.Lerp(rot, Q.O, t)), tm));
        }
        yield return Wf.Sec(tm);
        bots.ForEach(b => b.Follow());
    }
}
