using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class Bot : Character {
    [HideInInspector] public bool isFollow = false;
    bool isIn = false;
    float dSpd = 0;
    void Start() {
        Init();
    }
    void Update() {
        if (isFollow) {
            if (!isIn) {
                Tp = V3.Mv(Tp, Tp.X(0), Dt * 5);
                if (M.Abs(Tp.x) <= GC.roadR)
                    isIn = true;
            } else {
                Tp = Tp.X(M.C(Tp.x, -GC.roadR, GC.roadR));
            }
            Run();
            tf.LookAt(P.Tp);
            rb.V(TfF * (P.spd + dSpd));
            dSpd += Dt / 10;
        } else if (P.Tp.z - 20 > Tp.z) {
            Dst(go);
        }
    }
    public void Follow() {
        isFollow = true;
    }
    private void OnTriggerEnter(Collider other) {
        if (other.Tag(Tag.Dead) && IsGame) {
            if (other.name.IsS("Lightning")) {
                Dead(C.black);
            } else if (other.name.IsS("ShardIce")) {
                Dead(C.skyBlue);
            } else if (other.name.IsS("ShardMagic")) {
                Dead(C.purple);
            } else if (other.name.IsS("ShardRock")) {
                Dead(C.gray);
            } else if (other.name.IsS("ShardVine")) {
                Dead(C.darkOliveGreen);
            } else if (other.Par().name.IsS("FlameThrower")) {
                Dead(C.red);
            } else if (other.Par().name.IsS("Laser")) {
                Dead(C.black);
            } else if (other.Par(1).name.IsS("Explosion")) {
                Dead(C.black);
                foreach (var rb in rd.rbs)
                    rb.AddExplosionForce(10000, other.Tp(), 10);
            } else if (other.name.IsS("Saw")) {
                Dead();
            }
        }
    }
    private void OnCollisionEnter(Collision other) {
        if (other.Tag(Tag.Dead) && IsGame) {
            Obs obs = other.Par<Obs>();
            Dead();
            if (obs.tp == ObsTp.Glove) {
                Vector3 p = Tp + V3.Xy(obs.isR ? 3 : -3, -3);
                foreach (var rb in rd.rbs)
                    rb.AddExplosionForce(10000, p, 10);
            }
        }
    }
    public void Dead(Color c = default) {
        CC.RiseTxt(Tp, "+1");
        Gc.Coin++;
        isFollow = false;
        rb.ConFrzAll();
        rb.V0();
        rd.On();
        if (!c.IsDef())
            go.ChildGo(0, 0).RenMatCol(c);
    }
    public void Win() {
        isFollow = false;
        rb.ConFrzAll();
        rb.V0();
        rd.an.SetBool("Win", true);
    }
}
