using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public enum ObsTp {
    ShardIce, ShardIce2, ShardMagic, ShardMagic2, ShardRock, ShardRock2, ShardVine, ShardVine2,
    Lightning, Lightning2, FlameThrower, Laser, Glove, Explosion, Ball, Saw
}
public class Obs : Mb {
    public static List<ObsTp> Doors = Lis(ObsTp.ShardIce, ObsTp.ShardMagic, ObsTp.ShardRock, ObsTp.ShardVine, ObsTp.Lightning),
        Btns = Lis(ObsTp.FlameThrower, ObsTp.Laser, ObsTp.Glove, ObsTp.Explosion);
    [HideInInspector] public ObsTp tp;
    [HideInInspector] public bool isR;
    GameObject sawGo, modelGo;
    bool isMv;
    float t = 0, x0, x, r;
    private void Update() {
        if (P.Tp.z - 30 > Tp.z)
            Dst(go);
        if (sawGo) {
            t += Dt;
            modelGo.Tle(V3.V(t * -90, -90, -90));
            if (isMv) {
                x0 += r * Dt * 3;
                if (r > 0 ? x0 > x : x0 < -x) {
                    x0 = r * x;
                    r = -r;
                }
                sawGo.TlpX(x0);
            }
        }
    }
    public void Saw(int tp, float z, bool isR) {
        this.tp = ObsTp.Saw;
        this.isR = isR;
        isMv = tp < 2;
        r = isR ? 1 : -1;
        float sz = tp + 2;
        sawGo = go.ChildGo(0);
        sawGo.BcSz(V3.V(sz, sz, 0.5f));
        modelGo = sawGo.ChildGo(0);
        modelGo.Tls(V3.V(sz));
        go.Child(1).Tls(V3.V(isMv ? 10 : 4, 0.1f, 0.2f));
        Tp = V3.V(isMv ? 0 : (isR ? 2.5f : -2.5f), 0, z);
        if (isMv) {
            x = 5 - sz / 2;
            x0 = isR ? -x : x;
            sawGo.TlpX(x0);
        }
    }
    public void Init(ObsTp tp, float z, bool isR) {
        bool is2 = tp.tS().IsE("2");
        this.tp = tp;
        this.isR = isR;
        if (tp.tS().IsS("Lightning")) {
            Tp = V3.V(is2 ? 0 : (isR ? 2.5f : -2.5f), 0, z);
            Tr = Q.O;
            if (is2) {
                for (int i = 0; i < 9; i++)
                    Ins(GC.lightningStrikePf, Tp.X(i - 4.5f + Rnd.Rng(-0.5f, 0.5f)), Q.O, tf);
            } else {
                for (int i = 0; i < 5; i++)
                    Ins(GC.lightningStrikePf, Tp + Rnd.InUnitCircle.Xz() * 3, Q.O, tf);
            }
        } else if (tp.tS().IsS("Shard")) {
            Tp = V3.V(is2 ? (isR ? 5 : -5) : (isR ? 2.5f : -2.5f), tp.tS().IsS("ShardRock") ? 0.5f : tp.tS().IsS("ShardVine") ? 1 : 0, z);
            Tr = Q.Y(is2 ? (isR ? -90 : 90) : 0);
        } else if (tp == ObsTp.FlameThrower || tp == ObsTp.Laser || tp == ObsTp.Glove) {
            Tp = V3.Z(z);
            Tr = Q.Y(isR ? 180 : 0);
        } else {
            Tp = V3.Z(z);
            Tr = Q.O;
        }
        if (Btns.Contains(tp))
            Ins(GC.btnPf, Tp.X(tp == ObsTp.Explosion ? Rnd.RngIn(-1, 1) : Rnd.RngIn(-3, 3)) + V3.Z(3), Q.O, tf);
    }
}
