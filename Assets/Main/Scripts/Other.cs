using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class Other : Mb {
    public enum OtherTp { Rot, BtnShine, WaitShow, SinScl, TutHand, ColA, Fps }
    public OtherTp tp;
    [DrawIf(nameof(tp), OtherTp.Rot)] public Vector3 rot;
    [DrawIf(nameof(tp), OtherTp.BtnShine)] public Vector2 btnShine = V2.I;
    [DrawIf(nameof(tp), OtherTp.WaitShow)] public float waitShow;
    [DrawIf(nameof(tp), OtherTp.SinScl)] public Vector3 sinScl = V3.V(1, 1.05f, 1);
    [DrawIf(nameof(tp), OtherTp.TutHand)] public Vector3 tutHand = V3.V(200, 80, 3);
    [DrawIf(nameof(tp), OtherTp.ColA)] public float colA = 2;
    void OnEnable() {
        if (tp == OtherTp.Rot) StaCor(RotCor());
        else if (tp == OtherTp.BtnShine) StaCor(BtnShineCor());
        else if (tp == OtherTp.WaitShow) StaCor(WaitShowCor());
        else if (tp == OtherTp.SinScl) StaCor(SinSclCor());
        else if (tp == OtherTp.TutHand) StaCor(TutHandCor());
        else if (tp == OtherTp.ColA) StaCor(ColACor());
        else if (tp == OtherTp.Fps) StaCor(FpsCor());
    }
    IEnumerator RotCor() {
        while (true) {
            tf.RotateAround(Tp, TfR, Dt * rot.x);
            tf.RotateAround(Tp, TfU, Dt * rot.y);
            tf.RotateAround(Tp, TfF, Dt * rot.z);
            yield return null;
        }
    }
    IEnumerator BtnShineCor() {
        float x = -go.Tlp().x;
        while (true) {
            yield return Wf.Sec(btnShine.x);
            yield return Cor(t => go.TlpX(M.Lerp(-x, x, t)), btnShine.y);
        }
    }
    IEnumerator WaitShowCor(float tm = 0.3f) {
        Tls = V3.O;
        yield return Wf.Sec(waitShow);
        yield return Cor(t => Tls = V3.V(t), tm, EaseTp.OutQuart);
    }
    IEnumerator SinSclCor() {
        while (true)
            yield return Cor(t => Tls = V3.V(M.Lerp(sinScl.x, sinScl.y, M.Sin01(t * 360))), sinScl.z);
    }
    IEnumerator TutHandCor() {
        Vector3 p = go.Tlp();
        while (true)
            yield return Cor(t => go.Tlp(p + V3.Xy(M.Cos(t * 360) * tutHand.x, M.Sin(t * 720) * tutHand.y)), tutHand.z);
    }
    IEnumerator ColACor() {
        Text txt = go.Txt();
        while (true)
            yield return Cor(t => txt.ColA(M.Sin01(t * 360)), colA);
    }
    IEnumerator FpsCor() {
        Text txt = go.Txt();
        int frm = 0;
        for (float t = 0; true; t += Dt, frm++) {
            if (t >= 0.99f) {
                txt.text = "" + frm;
                t = frm = 0;
            }
            yield return null;
        }
    }
}
