using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

namespace KenneyCharacterAssets {
    public enum SkinTp2 {
        animal, animalBase, alien, astro, athlete, business, casual, criminal, cyborg,
        fantasy, farmer, military, racer, robot, skater, survivor, zombie
    }
    public enum SkinTp {
        animalB, animalC, animalE, animalG, animalH, animalI, animalA, animalD, animalF, animalJ,
        animalBaseB, animalBaseC, animalBaseE, animalBaseG, animalBaseH, animalBaseI, animalBaseA, animalBaseD, animalBaseF, animalBaseJ,
        alienA, alienB,
        astroMaleA, astroMaleB, astroFemaleA, astroFemaleB,
        athleteMaleBlue, athleteMaleGreen, athleteMaleRed, athleteMaleYellow, athleteFemaleBlue, athleteFemaleGreen, athleteFemaleRed, athleteFemaleYellow,
        businessMaleA, businessMaleB,
        casualMaleA, casualMaleB, casualFemaleA, casualFemaleB,
        criminalMaleA,
        cyborg, cyborgFemaleA,
        fantasyMaleA, fantasyMaleB, fantasyFemaleA, fantasyFemaleB,
        farmerA, farmerB,
        militaryMaleA, militaryMaleB, militaryFemaleA, militaryFemaleB,
        racerBlueMale, racerGreenMale, racerOrangeMale, racerPurpleMale, racerRedMale,
        racerBlueFemale, racerGreenFemale, racerOrangeFemale, racerPurpleFemale, racerRedFemale,
        robot, robot2, robot3,
        skaterMaleA, skaterFemaleA,
        survivorMaleA, survivorMaleB, survivorFemaleA, survivorFemaleB,
        zombieA, zombieB, zombieC,
    }
    public enum AccTp {
        earsA, earsB, earsC, earsD, earsE, mouthA, mouthB, tailA, tailB,
        astroHelmet, racingHelmet, militaryHelmet,
        cap, fantasyCap, farmerCap, militaryBeret, strawhat,
        glassesRetro, glassesRound,
        beard, hairBobcut, hairPigtail, hairPonytail, hairTail,
        fantasyShoulderL, fantasyShoulderR, militaryShoulderL, militaryShoulderR,
        fantasyArmguardL, fantasyArmguardR,
        astroBackpack, fantasyBackpack, militaryBackpack, modernBackpack,
        astroGun, pistol, rifle, sword, pitchfork, shield, fantasyHolster,
    }
    public enum MatTp {
        Black, Blonde, Blue_1, Blue_2, Blue_3, Brown_1, Brown_2, Brown_3,
        Emission, Green_1, Green_2, Orange_1, Orange_2, Orange_3,
        Steel_1, Steel_2, Steel_3, Steel_4, Steel_5, Tan_1, Tan_2, White
    }
    public enum AnimalTp { Mouse, Rabbit, Fox, Dog, Wolf, Bear, Puma, Cat }
    public enum AnimalColTp { Yellow, LightOrange, Orange, OrangeRed, Pink, White, LightGray, Gray, DarkGray, Brown }
    public static class Kenney {
        static Dictionary<SkinTp, MatTp> SkinDic = new Dictionary<SkinTp, MatTp>() {
            { SkinTp.animalA, MatTp.Orange_1 }, { SkinTp.animalB, MatTp.Orange_1 }, { SkinTp.animalC, MatTp.Green_1 }, { SkinTp.animalD, MatTp.Tan_1 }, { SkinTp.animalE, MatTp.Brown_1 }, { SkinTp.animalF, MatTp.Orange_3 }, { SkinTp.animalG, MatTp.Steel_2 }, { SkinTp.animalH, MatTp.Orange_3 }, { SkinTp.animalI, MatTp.Steel_5 }, { SkinTp.animalJ, MatTp.Steel_1 },
            { SkinTp.animalBaseA, MatTp.Orange_1 }, { SkinTp.animalBaseB, MatTp.Orange_1 }, { SkinTp.animalBaseC, MatTp.Green_1 }, { SkinTp.animalBaseD, MatTp.Tan_1 }, { SkinTp.animalBaseE, MatTp.Brown_1 }, { SkinTp.animalBaseF, MatTp.Orange_3 }, { SkinTp.animalBaseG, MatTp.Steel_2 }, { SkinTp.animalBaseH, MatTp.Orange_3 }, { SkinTp.animalBaseI, MatTp.Steel_5 }, { SkinTp.animalBaseJ, MatTp.Steel_1 },
            { SkinTp.alienA, MatTp.Steel_2 }, { SkinTp.alienB, MatTp.Tan_1 },
            { SkinTp.astroFemaleA, MatTp.Brown_3 }, { SkinTp.astroFemaleB, MatTp.Brown_3 }, { SkinTp.astroMaleA, MatTp.Brown_2 }, { SkinTp.astroMaleB, MatTp.Brown_3 },
            { SkinTp.athleteFemaleBlue, MatTp.Blonde }, { SkinTp.athleteFemaleGreen, MatTp.Orange_3 }, { SkinTp.athleteFemaleRed, MatTp.Brown_2 }, { SkinTp.athleteFemaleYellow, MatTp.Brown_3 }, { SkinTp.athleteMaleBlue, MatTp.Brown_3 }, { SkinTp.athleteMaleGreen, MatTp.Blonde }, { SkinTp.athleteMaleRed, MatTp.Brown_2 }, { SkinTp.athleteMaleYellow, MatTp.Brown_3 },
            { SkinTp.businessMaleA, MatTp.Blonde }, { SkinTp.businessMaleB, MatTp.Brown_3 },
            { SkinTp.casualFemaleA, MatTp.Brown_3 }, { SkinTp.casualFemaleB, MatTp.Orange_3 }, { SkinTp.casualMaleA, MatTp.Brown_2 }, { SkinTp.casualMaleB, MatTp.Brown_3 },
            { SkinTp.criminalMaleA, MatTp.Brown_3 },
            { SkinTp.cyborg, MatTp.Brown_3 }, { SkinTp.cyborgFemaleA, MatTp.Steel_2 },
            { SkinTp.fantasyFemaleA, MatTp.Orange_3 }, { SkinTp.fantasyFemaleB, MatTp.Blonde }, { SkinTp.fantasyMaleA, MatTp.Brown_3 }, { SkinTp.fantasyMaleB, MatTp.Brown_2 },
            { SkinTp.farmerA, MatTp.Blonde }, { SkinTp.farmerB, MatTp.Brown_3 },
            { SkinTp.militaryFemaleA, MatTp.Brown_2 }, { SkinTp.militaryFemaleB, MatTp.Blonde }, { SkinTp.militaryMaleA, MatTp.Brown_3 }, { SkinTp.militaryMaleB, MatTp.Brown_3 },
            { SkinTp.racerBlueFemale, MatTp.Brown_3 }, { SkinTp.racerBlueMale, MatTp.Brown_3 }, { SkinTp.racerGreenFemale, MatTp.Orange_3 }, { SkinTp.racerGreenMale, MatTp.Brown_3 }, { SkinTp.racerOrangeFemale, MatTp.Brown_2 }, { SkinTp.racerOrangeMale, MatTp.Blonde }, { SkinTp.racerPurpleFemale, MatTp.Brown_2 }, { SkinTp.racerPurpleMale, MatTp.Brown_3 }, { SkinTp.racerRedFemale, MatTp.Blonde }, { SkinTp.racerRedMale, MatTp.Brown_3 },
            { SkinTp.robot, MatTp.Steel_1 }, { SkinTp.robot2, MatTp.Steel_1 }, { SkinTp.robot3, MatTp.Steel_2 },
            { SkinTp.skaterFemaleA, MatTp.Brown_3 }, { SkinTp.skaterMaleA, MatTp.Brown_2 },
            { SkinTp.survivorFemaleA, MatTp.Brown_3 }, { SkinTp.survivorFemaleB, MatTp.Brown_2 }, { SkinTp.survivorMaleA, MatTp.Brown_3 }, { SkinTp.survivorMaleB, MatTp.Blonde },
            { SkinTp.zombieA, MatTp.Steel_2 }, { SkinTp.zombieB, MatTp.Brown_3 }, { SkinTp.zombieC, MatTp.Brown_3 },
        };
        static Dictionary<AccTp, string> AccDic = new Dictionary<AccTp, string>() {
            { AccTp.earsA, "1 0 1 0" }, { AccTp.earsB, "1 0 1 1" }, { AccTp.earsC, "1 0 1 2" }, { AccTp.earsD, "1 0 1 3" }, { AccTp.earsE, "1 0 1 4" }, { AccTp.mouthA, "1 0 1 5" }, { AccTp.mouthB, "1 0 1 6" }, { AccTp.tailA, "1 0 0 3 1" }, { AccTp.tailB, "1 0 0 3 2" },
            { AccTp.astroHelmet, "1 0 1 7" }, { AccTp.racingHelmet, "1 0 1 8" }, { AccTp.militaryHelmet, "1 0 1 9" },
            { AccTp.cap, "1 0 1 10" }, { AccTp.fantasyCap, "1 0 1 11" }, { AccTp.farmerCap, "1 0 1 12" }, { AccTp.militaryBeret, "1 0 1 13" }, { AccTp.strawhat, "1 0 1 14" },
            { AccTp.glassesRetro, "1 0 1 15" }, { AccTp.glassesRound, "1 0 1 16" },
            { AccTp.beard, "1 0 1 17" }, { AccTp.hairBobcut, "1 0 1 18" }, { AccTp.hairPigtail, "1 0 1 19" }, { AccTp.hairPonytail, "1 0 1 20" }, { AccTp.hairTail, "1 0 1 21" },
            { AccTp.fantasyShoulderL, "0 1 0" }, { AccTp.fantasyShoulderR, "2 1 0" }, { AccTp.militaryShoulderL, "0 1 1" }, { AccTp.militaryShoulderR, "2 1 1" },
            { AccTp.fantasyArmguardL, "0 0 0 1 0" }, { AccTp.fantasyArmguardR, "2 0 0 1 0" },
            { AccTp.astroBackpack, "3 0" }, { AccTp.fantasyBackpack, "3 1" }, { AccTp.militaryBackpack, "3 2" }, { AccTp.modernBackpack, "3 3" },
            { AccTp.astroGun, "2 0 0 0 2 0" }, { AccTp.pistol, "2 0 0 0 2 1" }, { AccTp.rifle, "2 0 0 0 2 2" }, { AccTp.sword, "2 0 0 0 2 3" }, { AccTp.pitchfork, "2 0 0 0 2 4" }, { AccTp.shield, "0 0 0 1 1" }, { AccTp.fantasyHolster, "1 0 0 3 0" },
        };
        static Dictionary<AnimalTp, AccTp[]> AnimalDic = new Dictionary<AnimalTp, AccTp[]>() {
            { AnimalTp.Mouse, Mb.Arr(AccTp.earsD, AccTp.mouthA, AccTp.tailA) },
            { AnimalTp.Rabbit, Mb.Arr(AccTp.earsE, AccTp.mouthA, AccTp.tailB) },
            { AnimalTp.Fox, Mb.Arr(AccTp.earsB, AccTp.mouthA, AccTp.tailA) },
            { AnimalTp.Dog, Mb.Arr(AccTp.earsA, AccTp.mouthA, AccTp.tailA) },
            { AnimalTp.Wolf, Mb.Arr(AccTp.earsA, AccTp.mouthB, AccTp.tailA) },
            { AnimalTp.Bear, Mb.Arr(AccTp.earsC, AccTp.mouthA, AccTp.tailB) },
            { AnimalTp.Puma, Mb.Arr(AccTp.earsC, AccTp.mouthA, AccTp.tailA) },
            { AnimalTp.Cat, Mb.Arr(AccTp.earsA, AccTp.tailA) },
        };
        static Dictionary<AnimalColTp, (SkinTp, SkinTp, AnimalTp)> AnimalColDic = new Dictionary<AnimalColTp, (SkinTp, SkinTp, AnimalTp)>() {
            { AnimalColTp.Yellow, (SkinTp.animalBaseA, SkinTp.animalA, AnimalTp.Mouse) },
            { AnimalColTp.LightOrange, (SkinTp.animalBaseB, SkinTp.animalB, AnimalTp.Fox) },
            { AnimalColTp.Orange, (SkinTp.animalBaseF, SkinTp.animalF, AnimalTp.Bear) },
            { AnimalColTp.OrangeRed, (SkinTp.animalBaseH, SkinTp.animalH, AnimalTp.Fox) },
            { AnimalColTp.Pink, (SkinTp.animalBaseD, SkinTp.animalD, AnimalTp.Rabbit) },
            { AnimalColTp.White, (SkinTp.animalBaseJ, SkinTp.animalJ, AnimalTp.Rabbit) },
            { AnimalColTp.LightGray, (SkinTp.animalBaseG, SkinTp.animalG, AnimalTp.Dog) },
            { AnimalColTp.Gray, (SkinTp.animalBaseC, SkinTp.animalC, AnimalTp.Dog) },
            { AnimalColTp.DarkGray, (SkinTp.animalBaseI, SkinTp.animalI, AnimalTp.Mouse) },
            { AnimalColTp.Brown, (SkinTp.animalBaseE, SkinTp.animalE, AnimalTp.Bear) },
        };
        static List<AccTp> animalAccs = Mb.Lis(AccTp.earsA, AccTp.earsB, AccTp.earsC, AccTp.earsD, AccTp.earsE, AccTp.mouthA, AccTp.mouthB, AccTp.tailA, AccTp.tailB),
            hairAccs = Mb.Lis(AccTp.beard, AccTp.hairBobcut, AccTp.hairPigtail, AccTp.hairPonytail, AccTp.hairTail),
            hipsAccs = Mb.Lis(AccTp.fantasyHolster, AccTp.tailA, AccTp.tailB);
        static List<SkinTp> females = Mb.Lis(SkinTp.animalA, SkinTp.animalD, SkinTp.animalF, SkinTp.animalJ, SkinTp.animalBaseA, SkinTp.animalBaseD, SkinTp.animalBaseF, SkinTp.animalBaseJ, SkinTp.zombieC);
        static Dictionary<MatTp, Material> MatDic = new Dictionary<MatTp, Material>();
        static void Init() {
            if (MatDic.Count == 0)
                for (MatTp tp = 0; tp <= MatTp.White; tp++)
                    MatDic.Add(tp, O.Load<Material>("Accessories/Materials/" + tp.tS().Rpl('_', ' ')));
        }
        public static void Medium(this GameObject go, SkinTp tp, params AccTp[] accs) {
            Init();
            Texture2D tex = SkinTex(tp);
            Material mat = go.Child(0).RenMat(), hairMat = MatDic[SkinDic[tp]];
            mat.color = C.I;
            mat.mainTexture = tex;
            foreach (var acc in AccDic.Keys)
                AccGo(go, acc).Hide();
            for (int i = 0; i < accs.Length; i++) {
                GameObject accGo = AccGo(go, accs[i]);
                if (animalAccs.Contains(accs[i])) {
                    accGo.RenMat().mainTexture = tex;
                } else if (hairAccs.Contains(accs[i])) {
                    accGo.Ren().materials[0] = hairMat;
                    if (accs[i] == AccTp.hairBobcut)
                        accGo.Ren().materials[1] = hairMat;
                }
                accGo.Show();
            }
        }
        public static void MediumAnimal(this GameObject go, AnimalColTp col, AnimalTp tp, bool isBase = true) { Medium(go, isBase ? AnimalColDic[col].Item1 : AnimalColDic[col].Item2, AnimalDic[tp]); }
        public static void MediumAnimal(this GameObject go, AnimalColTp col, bool isBase = true) { MediumAnimal(go, col, AnimalColDic[col].Item3, isBase); }
        public static void Acc(this GameObject go, AccTp acc, params MatTp[] mats) {
            Init();
            GameObject accGo = AccGo(go, acc);
            accGo.Show();
            Material[] arr = accGo.Ren().materials;
            for (int i = 0; i < mats.Length; i++)
                arr[i] = MatDic[mats[i]];
        }
        public static Texture2D SkinTex(this SkinTp tp) { return O.Load<Texture2D>("Skins/" + (tp.tS().IsS("animal") ? "Animals/" : "") + tp.tS()); }
        public static GameObject AccGo(this GameObject go, AccTp tp) { return go.ChildGo((hipsAccs.Contains(tp) ? "" : "1 0 0 2 0 0 ") + AccDic[tp]); }
        public static bool IsFemale(this SkinTp tp) { return females.Contains(tp) || tp.tS().Contains("Female"); }
        public static bool IsMale(this SkinTp tp) { return !IsFemale(tp); }
        public static SkinTp2 Tp2(this SkinTp tp) {
            if (tp <= SkinTp.animalJ) return SkinTp2.animal;
            else if (tp <= SkinTp.animalBaseJ) return SkinTp2.animalBase;
            else if (tp <= SkinTp.alienB) return SkinTp2.alien;
            else if (tp <= SkinTp.astroFemaleB) return SkinTp2.astro;
            else if (tp <= SkinTp.athleteFemaleYellow) return SkinTp2.athlete;
            else if (tp <= SkinTp.businessMaleB) return SkinTp2.business;
            else if (tp <= SkinTp.casualFemaleB) return SkinTp2.casual;
            else if (tp == SkinTp.criminalMaleA) return SkinTp2.criminal;
            else if (tp <= SkinTp.cyborgFemaleA) return SkinTp2.cyborg;
            else if (tp <= SkinTp.fantasyFemaleB) return SkinTp2.fantasy;
            else if (tp <= SkinTp.farmerB) return SkinTp2.farmer;
            else if (tp <= SkinTp.militaryFemaleB) return SkinTp2.military;
            else if (tp <= SkinTp.racerRedFemale) return SkinTp2.racer;
            else if (tp <= SkinTp.robot3) return SkinTp2.robot;
            else if (tp <= SkinTp.skaterFemaleA) return SkinTp2.skater;
            else if (tp <= SkinTp.survivorFemaleB) return SkinTp2.survivor;
            else return SkinTp2.zombie;
        }
        public static SkinTp RndTp(this SkinTp2 tp) {
            switch (tp) {
                case SkinTp2.animal: return Rnd.Enum(SkinTp.animalB, SkinTp.animalJ);
                case SkinTp2.animalBase: return Rnd.Enum(SkinTp.animalBaseB, SkinTp.animalBaseJ);
                case SkinTp2.alien: return Rnd.Enum(SkinTp.alienA, SkinTp.alienB);
                case SkinTp2.astro: return Rnd.Enum(SkinTp.astroMaleA, SkinTp.astroFemaleB);
                case SkinTp2.athlete: return Rnd.Enum(SkinTp.athleteMaleBlue, SkinTp.athleteFemaleYellow);
                case SkinTp2.business: return Rnd.Enum(SkinTp.businessMaleA, SkinTp.businessMaleB);
                case SkinTp2.casual: return Rnd.Enum(SkinTp.casualMaleA, SkinTp.casualFemaleB);
                case SkinTp2.criminal: return SkinTp.criminalMaleA;
                case SkinTp2.cyborg: return Rnd.Enum(SkinTp.cyborg, SkinTp.cyborgFemaleA);
                case SkinTp2.fantasy: return Rnd.Enum(SkinTp.fantasyMaleA, SkinTp.fantasyFemaleB);
                case SkinTp2.farmer: return Rnd.Enum(SkinTp.farmerA, SkinTp.farmerB);
                case SkinTp2.military: return Rnd.Enum(SkinTp.militaryMaleA, SkinTp.militaryFemaleB);
                case SkinTp2.racer: return Rnd.Enum(SkinTp.racerBlueMale, SkinTp.racerRedFemale);
                case SkinTp2.robot: return Rnd.Enum(SkinTp.robot, SkinTp.robot3);
                case SkinTp2.skater: return Rnd.Enum(SkinTp.skaterMaleA, SkinTp.skaterFemaleA);
                case SkinTp2.survivor: return Rnd.Enum(SkinTp.survivorMaleA, SkinTp.survivorFemaleB);
                default: return Rnd.Enum(SkinTp.zombieA, SkinTp.zombieC);
            }
        }
        public static SkinTp RndTp(this SkinTp2 tp, bool isMale) {
            switch (tp) {
                case SkinTp2.animal: return isMale ? Rnd.Enum(SkinTp.animalB, SkinTp.animalI) : Rnd.Enum(SkinTp.animalA, SkinTp.animalJ);
                case SkinTp2.animalBase: return isMale ? Rnd.Enum(SkinTp.animalBaseB, SkinTp.animalBaseI) : Rnd.Enum(SkinTp.animalBaseA, SkinTp.animalBaseJ);
                case SkinTp2.alien: return Rnd.Enum(SkinTp.alienA, SkinTp.alienB);
                case SkinTp2.astro: return isMale ? Rnd.Enum(SkinTp.astroMaleA, SkinTp.astroMaleB) : Rnd.Enum(SkinTp.astroFemaleA, SkinTp.astroFemaleB);
                case SkinTp2.athlete: return isMale ? Rnd.Enum(SkinTp.athleteMaleBlue, SkinTp.athleteMaleYellow) : Rnd.Enum(SkinTp.athleteFemaleBlue, SkinTp.athleteFemaleYellow);
                case SkinTp2.business: return Rnd.Enum(SkinTp.businessMaleA, SkinTp.businessMaleB);
                case SkinTp2.casual: return isMale ? Rnd.Enum(SkinTp.casualMaleA, SkinTp.casualMaleB) : Rnd.Enum(SkinTp.casualFemaleA, SkinTp.casualFemaleB);
                case SkinTp2.criminal: return SkinTp.criminalMaleA;
                case SkinTp2.cyborg: return isMale ? SkinTp.cyborg : SkinTp.cyborgFemaleA;
                case SkinTp2.fantasy: return isMale ? Rnd.Enum(SkinTp.fantasyMaleA, SkinTp.fantasyMaleB) : Rnd.Enum(SkinTp.fantasyFemaleA, SkinTp.fantasyFemaleB);
                case SkinTp2.farmer: return Rnd.Enum(SkinTp.farmerA, SkinTp.farmerB);
                case SkinTp2.military: return isMale ? Rnd.Enum(SkinTp.militaryMaleA, SkinTp.militaryMaleB) : Rnd.Enum(SkinTp.militaryFemaleA, SkinTp.militaryFemaleB);
                case SkinTp2.racer: return isMale ? Rnd.Enum(SkinTp.racerBlueMale, SkinTp.racerRedMale) : Rnd.Enum(SkinTp.racerBlueFemale, SkinTp.racerRedFemale);
                case SkinTp2.robot: return Rnd.Enum(SkinTp.robot, SkinTp.robot3);
                case SkinTp2.skater: return isMale ? SkinTp.skaterMaleA : SkinTp.skaterFemaleA;
                case SkinTp2.survivor: return isMale ? Rnd.Enum(SkinTp.survivorMaleA, SkinTp.survivorMaleB) : Rnd.Enum(SkinTp.survivorFemaleA, SkinTp.survivorFemaleB);
                default: return isMale ? Rnd.Enum(SkinTp.zombieA, SkinTp.zombieB) : SkinTp.zombieC;
            }
        }
    }
}